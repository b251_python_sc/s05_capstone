from abc import ABC, abstractmethod

class Person(ABC):
    @abstractmethod
    def getFullName(self):
        pass

    @abstractmethod
    def addRequest(self, request):
        pass

    @abstractmethod
    def checkRequest(self):
        pass

    @abstractmethod
    def addUser(self, user):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def getFullName(self):
        return self._firstName + " " + self._lastName

    def addRequest(self, request):
        pass

    def checkRequest(self):
        pass

    def addUser(self, user):
        pass

    def getFullname(self):
        return self._firstName + " " + self._lastName

    def login(self):
        return(f"{self._email} has logged in")
    def logout(self):
        return(f"{self._email} has logged out")

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []
    
    def get_members(self):
        return self._members

    def getFullName(self):
        return self._firstName + " " + self._lastName

    def addRequest(self, request):
        pass

    def checkRequest(self):
        pass

    def addUser(self, user):
        pass

    def login(self):
        print(f"{self._email} has logged in")
    def logout(self):
        print(f"{self._email} has logged out")

    def addMember(self, member):
        self._members.append(member)
    
    

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def getFullName(self):
        return self._firstName + " " + self._lastName

    def addRequest(self, request):
        pass

    def checkRequest(self):
        pass

    def addUser(self, user):
        print("new user added: ", user)

    def login(self):
        print(f"{self._email} has logged in")

    def logout(self):
        print(f"{self._email} has logged out")

class Request:
    def __init__(self, name, requester, dateRequested, status):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = status
    def getName(self):
        return self._name
    def setName(self,name):
        self._name = name
    def getRequester(self):
        return self._requester
    def setRequester(self,requester):
        self._requester = requester
    def getDateRequested(self):
        return self._dateRequested
    def setDateRequested(self,dateRequested):
        self._dateRequested = dateRequested
    def getStatus(self):
        return self._status
    def setStatus(self,status):
        self._status = status
    def updateRequest(self, new_name, new_status):
        self._name = new_name
        self._status = new_status
        print(f"Request {self._name} has been updated to status {self._status}")
    def closeRequest(self):
        self._status = "closed"
        print(f"Request {self._name} has been closed")
    def cancelRequest(self):
        self._status = "cancelled"
        print(f"Request {self._name} has been cancelled")

employee1 = Employee("Jherson", "Dignadice", "j.dignadice.27@gmail.com", "tech") 
employee2 = Employee("Lowes", "Smith", "smith@gmail.com", "tech") 
employee3 = Employee("Tim", "Roy", "tim_roy@gmail.com", "tech") 
employee4 = Employee("Eddy", "Gould", "eddy_gould@gmail.com", "tech") 
admin1 = Admin("Denise", "Bernas", "denise@gmail.com", "Development")
teamLead1 = TeamLead("Jhersy", "Dignadice", "jhersy@gmail.com", "marketing")
req1 = Request("new hire orientation", teamLead1, "June-21-2023", "active")
req2 = Request("Laptop Repair", employee1, "June-22-2023", "active")

assert employee1.getFullName() == "Jherson Dignadice", "Fullname should be Jherson Dignadice"
assert admin1.getFullName() == "Denise Bernas", "Fullname should be Denise Bernas"
assert teamLead1.getFullName() == "Jhersy Dignadice", "Fullname should be Jhersy Dignadice"
assert employee2.login() == "smith@gmail.com has logged in"
assert employee2.logout() == "smith@gmail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee2)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullname())

## assert admin1.addUser(user) == "User has been added"

req2.setStatus("closed")
print(req2.closeRequest())